//
//  Ball.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 04/12/2017.
//
//

#include "Ball.hpp"

void Ball::ballForwards(int speed)
{
        ballX = ballX + speed;
}

void Ball::ballBackwards(int speed)
{
        ballX = ballX - speed;
       
}

void Ball::paint (Graphics& g)
{
    g.drawRoundedRectangle(ballX, ballY, 10, 10, 30, 10);
}

