/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"




/** Class containing all audio processes */

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    void setGain (float newGain)
    {
        gain = newGain;
    }
    
    void setFrequencyZero ()
    {
        frequency = 0;
    }
    
    String getNote()
    {
        return noteName;
    }
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void newFrequency();
    
private:
    AudioDeviceManager audioDeviceManager;
    Atomic<float> gain;
    Atomic<float> frequency;
    float phasePosition;
    float sampleRate;
    int noteGen;
    String noteName;
    
};

#endif  // AUDIO_H_INCLUDED
