//
//  gameInterface.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#include "gameInterface.hpp"

void GameInterface::paint (Graphics &g)
{
    int pCounter = 780;
    g.setFont(12);
    
    for (int count = 2; count < 5; count ++)                                            /** Display note names on left side, Future cleaner loop (Future enum)**/
    {
        g.drawSingleLineText((String::formatted ("C%d", count)), 15, pCounter);
        g.drawSingleLineText((String::formatted ("C#%d", count)), 15, pCounter - 20);
        g.drawSingleLineText((String::formatted ("D%d", count)), 15, pCounter - 40);
        g.drawSingleLineText((String::formatted ("D#%d", count)), 15, pCounter - 60);
        g.drawSingleLineText((String::formatted ("E%d", count)), 15, pCounter - 80);
        g.drawSingleLineText((String::formatted ("F%d", count)), 15, pCounter - 100);
        g.drawSingleLineText((String::formatted ("F#%d", count)), 15, pCounter - 120);
        g.drawSingleLineText((String::formatted ("G%d", count)), 15, pCounter - 140);
        g.drawSingleLineText((String::formatted ("G#%d", count)), 15, pCounter - 160);
        g.drawSingleLineText((String::formatted ("A%d", count)), 15, pCounter - 180);
        g.drawSingleLineText((String::formatted ("A#%d", count)), 15, pCounter - 200);
        g.drawSingleLineText((String::formatted ("B%d", count)), 15, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
    
    pCounter = 780;
    
    for (int count = 2; count < 5; count ++)                                            /** Display note names on right side, Future cleaner loop (Future enum)**/
    {
        g.drawSingleLineText((String::formatted ("C%d", count)), 1043, pCounter);
        g.drawSingleLineText((String::formatted ("C#%d", count)), 1043, pCounter - 20);
        g.drawSingleLineText((String::formatted ("D%d", count)), 1043, pCounter - 40);
        g.drawSingleLineText((String::formatted ("D#%d", count)), 1043, pCounter - 60);
        g.drawSingleLineText((String::formatted ("E%d", count)), 1043, pCounter - 80);
        g.drawSingleLineText((String::formatted ("F%d", count)), 1043, pCounter - 100);
        g.drawSingleLineText((String::formatted ("F#%d", count)), 1043, pCounter - 120);
        g.drawSingleLineText((String::formatted ("G%d", count)), 1043, pCounter - 140);
        g.drawSingleLineText((String::formatted ("G#%d", count)), 1043, pCounter - 160);
        g.drawSingleLineText((String::formatted ("A%d", count)), 1043, pCounter - 180);
        g.drawSingleLineText((String::formatted ("A#%d", count)), 1043, pCounter - 200);
        g.drawSingleLineText((String::formatted ("B%d", count)), 1043, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
}
