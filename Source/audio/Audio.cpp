/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio() : gain (0.f)
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    float Position;
    
    
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency.get())/sampleRate;
    
    
    
    
    while(numSamples--)
        {
            phasePosition = phasePosition + phaseIncrement;
        
            if (phasePosition > twoPi)
            {
                phasePosition = phasePosition - twoPi;
            }
            Position = sin(phasePosition);
        
            *outL = Position * gain.get();
            *outR = Position * gain.get();
        
            inL++;
            inR++;
            outL++;
            outR++;
        }
    
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)                 
{
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
    srand(time(NULL));
}

void Audio::audioDeviceStopped()
{

}

void Audio::newFrequency()
{
    noteGen = (rand() % 36) + 48;
    frequency = 440 * pow(2, (noteGen - 69.0)/12.0);
    int pitchClass = noteGen % 12;
    int octave = (noteGen / 12) - 2;
    const StringArray notes = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    noteName = notes[pitchClass] + String (octave);
}



