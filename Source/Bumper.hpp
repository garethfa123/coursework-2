//
//  Bumper.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 10/01/2018.
//
//

#ifndef Bumper_hpp
#define Bumper_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


class Bumper   : public Component

{
public:
    //==============================================================================
    /** Constructor */
    Bumper() {}
    
    /** Destructor */
    ~Bumper() {}
    
   void setBumperY(int bumperYPos)
    {
        bumperY = bumperYPos;
    }
    
    
    void bumperPos();
    
private:
    int bumperY = 425;
    int bumperX = 64;
    
};


#endif /* Bumper_hpp */
