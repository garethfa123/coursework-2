/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
/////////////STILL TO DO///////////////
//Bounce off walls instead of going straight to note
//Sort out files/organisation
//Add midi sounds instead of sine tone
//Make too slow only work if the answer is correct, not any answer
//Look at enum

MainComponent::MainComponent (Audio& a) : audio (a)
{
    setSize (1300, 800);
    
    addAndMakeVisible(speedSlider);                                             /** Speed Slider **/
    speedSlider.setRange(1, 10, 1);
    speedSlider.addListener (this);
    
    addAndMakeVisible(gainSlider);                                              /** Gain Slider **/
    gainSlider.addListener (this);
    gainSlider.setRange(0, 2);
    gainSlider.setValue(0.4);
    gainSlider.setSliderStyle(juce::Slider::LinearVertical);
    
    addAndMakeVisible(submitButton);                                            /** Submit Button **/
    submitButton.addListener (this);
    submitButton.setButtonText ("Submit");
    
    addAndMakeVisible(startButton);                                             /** Start Button **/
    startButton.addListener (this);
    startButton.setButtonText ("Start");
    
    resetButton.addListener (this);                                             /** Reset Button **/
    resetButton.setButtonText ("Reset");
    
    addAndMakeVisible(answerBox);                                               /** Answer box **/
    
    addAndMakeVisible(instrumentSelector);                                        /** Instrument Selector **/
    instrumentSelector.addItem ("Piano", 1);
    instrumentSelector.addItem ("Guitar", 2);
    instrumentSelector.addItem ("Flute", 3);
    
    
    
    addAndMakeVisible(prevAnswer);                                                 /** Previous answer box **/
    prevAnswer.setReadOnly(true);
    
    correctAnswer.setReadOnly(true);                                                /** Correct answer box **/
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    speedSlider.setBounds (50,20, 983, 40);                                     /** Setting bounds **/
    gainSlider.setBounds (1230,250, 30, 250);
    submitButton.setBounds (1120,400, 100, 100);
    startButton.setBounds (1120,250, 100, 100);
    resetButton.setBounds (510,375, 100, 100);
    answerBox.setBounds ( 1120, 350, 100,50);
    prevAnswer.setBounds ( 1120, 100, 100,50);
    correctAnswer.setBounds ( 644, 327, 30,30);
    instrumentSelector.setBounds(1120, 200, 100, 50);
}

void MainComponent::paint (Graphics &g)
{
    g.setColour(Colours::greenyellow);                                          /** Display Area **/
    g.fillRoundedRectangle(40,60,1000,728,10);
    
    g.setColour(Colours::black);                                                /** Dislay Ball/Bumpers **/
    g.drawRoundedRectangle(rectX, rectY, 10, 10, 30, 10);
    g.fillRect(40, bumperY - 20, 20, 50);
    g.fillRect(1020, bumperY - 20, 20, 50);
    
    g.drawSingleLineText("Current Answer:", 1120, 90);                          /** Display text **/
    
    g.setFont(30);
    g.drawSingleLineText((String::formatted ("Score: %d", score)), 1120, 600);
    g.drawSingleLineText((String::formatted ("Highscore: %d", highscore)), 1120, 630);
    
    if (resetGame == 1)                                                         /** Paints if reset has been triggered **/
    {
        g.setFont(50);
        
        if (slow == true)                                                               /** Display game over if incorrect answer or too slow **/
        {
            g.drawSingleLineText((String::formatted ("Too Slow! Game Over!")), 360, 300);
        }
        
        else
        {
            g.drawSingleLineText((String::formatted ("Game Over!")), 450, 300);
        }
        
        g.setFont(30);
        g.drawSingleLineText((String::formatted ("The note was: ")), 470, 350);         /** Display correct answer **/
        
        addAndMakeVisible(correctAnswer);                                               /** Makes reset/correct answer sppear **/
        addAndMakeVisible(resetButton);
        
        if (score > highscore)                                                          /** Display highscore message **/
        {
            g.setFont(50);
            g.drawSingleLineText((String::formatted ("New Highscore!")), 410, 520);
        }
        
        resetGame = 0;
    }
    
    if (correct == 1)                                                                   /** Display correct message if answer is correct **/
    {
        g.setFont(50);
        g.drawSingleLineText((String::formatted ("Correct!")), 450, 300);
    }
    
    gameInterface.paint(g);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    audio.setGain (gainSlider.getValue());                                  /** Set gain to the value of the gain slider **/
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &startButton && isTimerRunning() == false)                /** Starts game **/
    {
        xPixelNo = rand() % 400 + 337;
        forwards = 1;
        startTimerHz(50);
        audio.newFrequency();
    }
    
    if (button == &submitButton)                                            /** Submits answer **/
    {
        prevAnswer.setText(answerBox.getText());
        
        if (answerBox.getTextValue() == audio.getNote())
        {
            correct = 1;
            incorrect = 0;
            answerBox.clear();
        }
        
        else
        {
            incorrect = 1;
            correct = 0;
            answerBox.clear();
        }
    }
    
    if (button == &resetButton)                                             /** Resets game **/
    {
        rectX = 64;
        rectY = 425;
        bumperY = 425;
        
        if (score > highscore)                                              /** If current score is greater than previous highscore, update highscore **/
        {
            highscore = score;
        }
        
        score = 0;
        
        prevAnswer.clear();
        resetButton.setVisible(false);
        correctAnswer.setVisible(false);
        timerCallback();
        repaint();
    }
}

void MainComponent::timerCallback()
{
    yNoteName();                                                            /** Calls function to check if note given is correct **/
    
    if (rectX <= 64 && correct == 1)                                        /** If correct and ball is on the left, give new freq and start moving right **/
    {
        xPixelNo = rand() % 400 + 337;
        forwards = 1;
        backwards = 0;
        correct = 0;
        score++;
        
        audio.newFrequency();
        answerBox.clear();
        prevAnswer.clear();
    }
    
    if (rectX >= 1010 && correct == 1)                                      /** If correct and ball is on the right, give new freq and start moving left **/
    {
        xPixelNo = rand() % 400 + 337;
        forwards = 0;
        backwards = 1;
        correct = 0;
        score++;
        
        audio.newFrequency();
        answerBox.clear();
        prevAnswer.clear();
    }
    
    if (rectX > 1020 || rectX < 50)                                         /** If correct answer isn't given, reset **/
    {
        correctAnswer.setText(currentNote);
        reset();
    }
    
    if (correct == 1)                                                       /** Moves bumpers if answer is correct **/
    {
        if (bumperY < pixelNo)
        {
            bumperY = bumperY + 10;
        }
        
        if (bumperY > pixelNo)
        {
            bumperY = bumperY - 10;
        }
    }
    
    if (forwards == 1)                                                      /** moves ball right **/ //ball.ballForwards(speedSlider.getValue());
    {
       rectX = rectX + speedSlider.getValue();
    }
    
    if (backwards == 1)                                                     /** moves ball left **/ // ball.ballBackwards(speedSlider.getValue());
    {
      rectX = rectX - speedSlider.getValue();
    }
   
    if (rectY <= 60 || rectY < pixelNo)                                     /** sets ball to go down **/
    {
        up = 0;
        down = 1;
    }
    
    if (rectY >= 770 || rectY > pixelNo)                                    /** sets ball to go up **/
    {
        up = 1;
        down = 0;
    }
    
    if (down == 1)
    {
        
        if (forwards == 1)
        {
            xDiff = xPixelNo - rectX;
            
            DBG ("xdiff: " << xDiff);
        }
        if (backwards == 1)
        {
            xDiff = rectX - xPixelNo;
            DBG ("xdiff: " << xDiff);
        }
        
    }
    
    if (up == 1)
    {
        if (forwards == 1)
        {
            xDiff = xPixelNo - rectX;
            DBG ("xdiff: " << xDiff);
        }
        if (backwards == 1)
        {
            xDiff = rectX - xPixelNo;
            DBG ("xdiff: " << xDiff);
        }
    }
    
    
    
    
    if (down == 1 && rectY < pixelNo)
    {
        if (rectY > pixelNo - 8 && rectY < pixelNo + 8)                     /** Keeps ball steady **/
        {
            rectY = pixelNo;
        }
        
        else if (rectY < pixelNo)                                          /** moves ball down **/
        {
            rectY = rectY + speedSlider.getValue();
        }
    }
    
    if (up == 1 && rectY > pixelNo)
    {
        
        if (rectY > pixelNo - 8 && rectY < pixelNo + 8)                     /** Keeps ball steady **/
        {
            rectY = pixelNo;
        }
        
        else if (rectY > pixelNo)                                           /** moves ball up **/
        {
            rectY = rectY - speedSlider.getValue();
        }
    }
    
    if (up == 1 && bumperY - rectY > 25 && (rectX >= 1010 || rectX <= 64))          /** If bumper hasn't moved in time, make answer incorrect **/
    {
        incorrect = 1;
        correct = 0;
        slow = true;
    }
    
    else if (down == 1 && rectY - bumperY > 25 && (rectX >= 1010 || rectX <= 64))
    {
        incorrect = 1;
        correct = 0;
        slow = true;
    }
    
    repaint();
}

void MainComponent::yNoteName()
{
    int pCounterY = 775;
    currentNote = audio.getNote();
                                                                                                                    //const StringArray notes = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    
    if (currentNote.startsWithChar('C') && currentNote.endsWithChar('2') && currentNote.length() == 2)              /** Sets pixel for ball to go to for each note **/
    {                                                                                                               /** Octave 2 **/
        pixelNo = pCounterY;                                                                                        /** Future own class **/
    }
    
    if (currentNote.startsWithChar('C') && currentNote.containsChar('#') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 20;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.endsWithChar('2') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 40;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.containsChar('#') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 60;
    }
    
    if (currentNote.startsWithChar('E') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 80;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.endsWithChar('2') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 100;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.containsChar('#') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 120;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.endsWithChar('2') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 140;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.containsChar('#') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 160;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.endsWithChar('2') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 180;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.containsChar('#') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 200;
    }
    
    if (currentNote.startsWithChar('B') && currentNote.endsWithChar('2'))
    {
        pixelNo = pCounterY - 220;
    }
    
    pCounterY = pCounterY - 240;
    
    if (currentNote.startsWithChar('C') && currentNote.endsWithChar('3') && currentNote.length() == 2)              /** Octave 3 **/
    {
        pixelNo = pCounterY;
    }
    
    if (currentNote.startsWithChar('C') && currentNote.containsChar('#') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 20;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.endsWithChar('3') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 40;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.containsChar('#') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 60;
    }
    
    if (currentNote.startsWithChar('E') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 80;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.endsWithChar('3') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 100;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.containsChar('#') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 120;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.endsWithChar('3') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 140;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.containsChar('#') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 160;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.endsWithChar('3') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 180;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.containsChar('#') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 200;
    }
    
    if (currentNote.startsWithChar('B') && currentNote.endsWithChar('3'))
    {
        pixelNo = pCounterY - 220;
    }
    
    pCounterY = pCounterY - 240;
    
    if (currentNote.startsWithChar('C') && currentNote.endsWithChar('4') && currentNote.length() == 2)              /** Octave 4 **/
    {
        pixelNo = pCounterY;
    }
    
    if (currentNote.startsWithChar('C') && currentNote.containsChar('#') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 20;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.endsWithChar('4') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 40;
    }
    
    if (currentNote.startsWithChar('D') && currentNote.containsChar('#') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 60;
    }
    
    if (currentNote.startsWithChar('E') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 80;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.endsWithChar('4') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 100;
    }
    
    if (currentNote.startsWithChar('F') && currentNote.containsChar('#') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 120;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.endsWithChar('4') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 140;
    }
    
    if (currentNote.startsWithChar('G') && currentNote.containsChar('#') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 160;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.endsWithChar('4') && currentNote.length() == 2)
    {
        pixelNo = pCounterY - 180;
    }
    
    if (currentNote.startsWithChar('A') && currentNote.containsChar('#') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 200;
    }
    
    if (currentNote.startsWithChar('B') && currentNote.endsWithChar('4'))
    {
        pixelNo = pCounterY - 220;
    }
    
    pCounterY = pCounterY - 240;
}

void MainComponent::reset()
{
    forwards = 0;                                   /** resets game **/                 //ball.getBallSpeed();
    backwards = 0;
    resetGame = 1;
    slow = false;
 
    audio.setFrequencyZero();
    stopTimer();
    repaint();
}
