//
//  gameInterface.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#ifndef gameInterface_hpp
#define gameInterface_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class GameInterface   :     public Component

{
public:
    //==============================================================================
    /** Constructor */
    GameInterface() {}
    
    /** Destructor */
    ~GameInterface() {}
    
   void paint (Graphics& g) override;
    
    
private:

    
};


#endif /* gameInterface_hpp */
