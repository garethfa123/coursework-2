//
//  Ball.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 04/12/2017.
//
//

#ifndef Ball_hpp
#define Ball_hpp

#include "../../JuceLibraryCode/JuceHeader.h"



class Ball   : public Component,
               public Timer
{
public:
    //==============================================================================
    /** Constructor */
    Ball()
    {
        
    }
    /** Destructor */
    ~Ball() {}
    
    int getBallSpeed()
    {
        return ballSpeed;
    }
    
    int getBallX()
    {
        return ballX;
    }
    
    int getBallY()
    {
        return ballY;
    }
    
    void setBallX(int xPos)
    {
        ballX = xPos;
    }
    
    void setBallY(int yPos)
    {
        ballY = yPos;
    }
    
    void ballForwards(int speed);
    
    void ballBackwards(int speed);
    
    void timerCallback() override {}
    
    void paint (Graphics& g) override;
    
private:
    int ballSpeed;
    int ballX = 64;
    int ballY = 425;
};


#endif /* Ball_hpp */
