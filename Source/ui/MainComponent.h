/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../Ball/Ball.hpp"
#include "../Bumper.hpp"
#include "../gameInterface/gameInterface.hpp"


//==============================================================================
/* This component lives inside our window, and this is where you should put all
    your controls and content. */
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Slider::Listener,
                        public Button::Listener,
                        
                        public Timer



{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    int getPixelNo()
    {
        return pixelNo;
    }
    
    void resized() override;
    
    void paint (Graphics& g) override;
    
    void sliderValueChanged(Slider* slider) override;
    
    void buttonClicked (Button* button) override;

    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    void BallMovement();
    void timerCallback() override;
    void yNoteName();
    void reset();
private:
    Audio& audio;
    Ball ball;
    Bumper bumper;
    GameInterface gameInterface;
    Slider speedSlider;
    Slider gainSlider;
    TextButton submitButton;
    TextButton startButton;
    TextButton resetButton;
    TextEditor answerBox;
    TextEditor prevAnswer;
    TextEditor correctAnswer;
    ComboBox instrumentSelector;
    int rectX = 64, rectY = 425;
    int bumperX = 64, bumperY = 425;
    int forwards = 0, backwards = 0, up = 0, down = 0;
    int pixelNo = 0;
    int endY = 0;
    int correct = 0, incorrect = 1;
    int resetGame = 0;
    int score = 0;
    int highscore = 0;
    int xPixelNo = 0;
    int xDiff;
    bool slow;
    String currentNote;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED


